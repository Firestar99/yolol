package yolol.compiler.incremental;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface SimpleQueue<E> extends Iterable<E> {
	
	/**
	 * add a new element e to the {@link SimpleQueue}.
	 *
	 * @param e the new element to add
	 * @return true if adding was successful
	 */
	boolean add(E e);
	
	/**
	 * adds multiple elements to the {@link SimpleQueue}.
	 *
	 * @param collection the elements to add
	 * @return the amount of elements added
	 */
	default int addCollection(Collection<E> collection) {
		Iterator<E> iter = collection.iterator();
		for (int i = 0; iter.hasNext(); i++)
			if (!add(iter.next()))
				return i;
		return collection.size();
	}
	
	default int addArray(E[] collection) {
		return addArray(collection, 0, collection.length);
	}
	
	/**
	 * adds multiple elements to the {@link SimpleQueue}.
	 *
	 * @param collection the elements to add
	 * @return the amount of elements added
	 */
	default int addArray(E[] collection, int offset, int count) {
		for (int i = 0; i < count; i++)
			if (!add(collection[offset + i]))
				return i;
		return count;
	}
	
	/**
	 * removes an element from the {@link SimpleQueue} and returns it
	 *
	 * @return the removed element
	 */
	@Nullable E remove();
	
	/**
	 * removes count elements from the {@link SimpleQueue} and puts them in the supplied array
	 *
	 * @param array  the array to fill
	 * @param offset the offset to start writing in the array
	 * @param count  the count of elements to remove
	 * @return the count of removed elements
	 */
	default int removeArray(E[] array, int offset, int count) {
		for (int i = 0; i < count; i++) {
			E e = remove();
			if (e == null)
				return i - 1;
			array[offset + i] = e;
		}
		return count;
	}
	
	/**
	 * removes count elements from the {@link SimpleQueue} and returns them in a {@link Collection} of maximum size #count.
	 * NOTE: the collection will be tinier than #count if there are no more elements to remove.
	 *
	 * @param count the count of elements to remove
	 * @return the removed elements as a {@link Collection}
	 */
	default Collection<@NotNull E> removeCollection(int count) {
		ArrayList<E> ret = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			E e = remove();
			if (e == null)
				break;
			ret.add(e);
		}
		return ret;
	}
	
	/**
	 * Returns true if the Queue has an entry.
	 * <b>If other operations are done on the queue it should be assumed that the result is immediately invalid.</b>
	 * <b>{@link HighlyConcurrentSimpleQueue} will always return true due to the usecase of that Queue</b>
	 *
	 * @return true if the queue has an entry
	 */
	boolean hasEntry();
	
	/**
	 * Gets the size of the Queue. For debug, monitor and testing only.
	 * <b>If other operations are done on the queue it should be assumed that the result is immediately invalid.</b>
	 * <b>May throw an {@link UnsupportedOperationException} if not implemented.</b>
	 *
	 * @return the size of the Queue
	 * @throws UnsupportedOperationException if not supported by the queue
	 * @see #hasEntry() as an always implemented function
	 */
	default int size() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}
	
	@NotNull
	@Override
	default Iterator<E> iterator() {
		return new Iterator<E>() {
			@Nullable E cache;
			
			public @Nullable E poll() {
				if (cache == null)
					cache = SimpleQueue.this.remove();
				return cache;
			}
			
			@Override
			public boolean hasNext() {
				return poll() != null;
			}
			
			@Override
			public E next() {
				E poll = poll();
				if (poll == null)
					throw new NoSuchElementException();
				cache = null;
				return poll;
			}
		};
	}
	
	@Override
	default Spliterator<E> spliterator() {
		return Spliterators.spliteratorUnknownSize(iterator(), 0);
	}
	
	default Stream<E> stream() {
		return StreamSupport.stream(spliterator(), false);
	}
	
	default Stream<E> parallelStream() {
		return StreamSupport.stream(spliterator(), true);
	}
}
