# CollisionAvoidance

| Name          | Description                                          | Device                       |
|---------------|------------------------------------------------------|------------------------------|
| :CollAvoid    | Turn on / off CA                                     | Memory Relay -> CAO, Button  |
| :CAO          | Turn on / off CA                                     | CollAvoid -> Memory Relay    |
| :CAD          | Rangefinder distance (range)                         | Memory Relay -> careserved   |
| :CASUD        | FCU speed for up/down                                | Memory Relay -> careserved   |
| :CASRL        | FCU speed for right/left                             | Memory Relay -> careserved   |
| :CASUDA       | FCU speed for up/down while approaching, negative    | Memory Relay -> careserved   |
| :CASRLA       | FCU speed for right/left while approaching, negative | Memory Relay -> careserved   |
| :CAOUD        | FCU up down, but shorter                             | Memory Relay -> FcuUpDown    |
| :CAORL        | FCU right left, but shorter                          | Memory Relay -> FcuRightLeft |
| :CAOF         | FCU Forward, but shorter                             | Memory Relay -> FcuForward   |
| :FcuUpDown    | FCU up down                                          | :CAOUD -> Memory Relay, FCU  |
| :FcuRightLeft | FCU right left                                       | :CAORL -> Memory Relay, FCU  |
| :FcuForward   | FCU forward                                          | :CAOF -> Memory Relay, FCU   |
| :CAT          | Rangefinder Top                                      |                              |
| :CATR         | Rangefinder Top Right                                |                              |
| :CATL         | Rangefinder Top Left                                 |                              |
| :CAB          | Rangefinder Bottom                                   |                              |
| :CABR         | Rangefinder Bottom Right                             |                              |
| :CABL         | Rangefinder Bottom Left                              |                              |
| :CAR          | Rangefinder Right                                    |                              |
| :CAL          | Rangefinder Left                                     |                              |
