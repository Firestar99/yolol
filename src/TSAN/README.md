| Name      | Device                      | Field       | Description                                           |
|-----------|-----------------------------|-------------|-------------------------------------------------------|
| :comk     | Memory chip                 |             | Communication secret key                              |
| :tr       | Memory chip                 |             | TSAN reset, set to 20 for ~5 seconds to reset         |
| :t\[xyz\] | Memory chip                 |             | TSAN calculated XYZ coordinate output                 |
| :t\[12\]i | Torpedo Laser Sensor \[12\] | Identifier  | torpedo distance measurement, target laser designator |
| :t\[12\]d | Torpedo Laser Sensor \[12\] | MissileDist | torpedo distance measurement, distance to "missile"   |
| :t\[12\]x | Torpedo Laser Sensor \[12\] | OffsetX     | torpedo distance measurement, offsetx for rotation    |
| :t\[12\]y | Torpedo Laser Sensor \[12\] | OffsetY     | torpedo distance measurement, offsety for rotation    |



