# Compass 2

### Globals

| Global Name       | RD* | Local Name | Device *2             | Field            | Description                                                                 |
|-------------------|-----|------------|-----------------------|------------------|-----------------------------------------------------------------------------|
|                   |     | :r         |                       |                  | reset, 0 = normal, 20 = reset                                               |
|                   |     | :q         |                       |                  | codeQueue q                                                                 |
| :\[abcd\]         | ->  | :r\[abcd\] | Navigation Receiver   | SignalStrength   | Navigation receiver setup like ISAN Quad                                    |
| :\[abcd\]t        | <-  | :t\[abcd\] | Navigation Receiver   | TargetMessage    | Navigation receiver setup like ISAN Quad                                    |
|                   |     | :\[abcd\]  |                       |                  | presquared ISAN distances                                                   |
|                   |     | :\[xyz\]   |                       |                  | ISAN position                                                               |
| :GyroCalibrate    | <-  | :gc        | Gyro sensor           | GyroCalibrate    | Gyro sensor calibrate                                                       |
| :GyroPitch        | ->  | :gp        | Gyro sensor           | GyroPitch        | Gyro sensor pitch                                                           |
| :GyroYaw          | ->  | :gy        | Gyro sensor           | GyroYaw          | Gyro sensor yaw                                                             |
| :GyroRoll         | ->  | :gr        | Gyro sensor           | GyroRoll         | Gyro sensor roll                                                            |
|                   |     | :s\[yrp\]  |                       |                  | sinus of yaw / roll / pitch                                                 |
|                   |     | :c\[yrp\]  |                       |                  | cosinus of yaw / roll / pitch                                               |
|                   |     | :w\[xyz\]  |                       |                  | Waypoint that should be targeted in ISAN space                              |
|                   |     | :l\[xyz\]  |                       |                  | normalized vector from ship :xyz to waypoint :w in ISAN space               |
| :kt               | ->  | :l         |                       |                  | accumulator of :ll                                                          |
| :kl               | <-  | :ll        |                       |                  | distance to waypoint, length of unnormalized :l vector                      |
|                   |     | :t\[12\]   |                       |                  | intermediary terms for matrix-vector multiplication, see src                |
|                   |     | :t\[xyz\]  |                       |                  | intermediary vector, result of gyro matrix-vector multiplication            |
|                   |     | :f\[xyz\]  |                       |                  | calibration matrix, forward vector                                          |
|                   |     | :r\[xyz\]  |                       |                  | calibration matrix, right vector                                            |
|                   |     | :d\[xyz\]  |                       |                  | calibration matrix, down vector                                             |
| :ok\[xyz\]        | <-  | :o\[xyz\]  |                       |                  | Waypoint in ship space normalized, result of calibration matrix-vector mul. |
|                   |     | :o\[ab\]   |                       |                  | Grid display cursor xy position                                             |
|                   |     | :p\[ab\]   |                       |                  | Grid display cursor previous xy position                                    |
|                   |     | :oc        |                       |                  | Grid display cursor character                                               |
|                   |     | :s         |                       |                  | Speed of the ship                                                           |
|                   |     | :t         |                       |                  | Tightness of the display                                                    |
| :SelectedLayer    | <-  | :il        | Advanced Grid Display | SelectedLayer    |                                                                             |
| :Cursor\[XY\]     | <-  | :i\[xy\]   | Advanced Grid Display | Cursor\[XY\]     |                                                                             |
| :ClearLayerGrid   | <-  | :ic        | Advanced Grid Display | ClearLayerGrid   |                                                                             |
| :Input            | <-  | :ii        | Advanced Grid Display | Input            |                                                                             |
| :GridLayerTextHue | <-  | :ih        | Advanced Grid Display | GridLayerTextHue |                                                                             |
| :MoveCursor       | <-  | :im        | Advanced Grid Display | MoveCursor       |                                                                             |
|                   |     | :mc        |                       |                  | Main bus activate calibrate                                                 |
|                   |     | :mr        |                       |                  | Main bus request reset                                                      |
| :ko               | <-  | :o         |                       |                  | outputxyz text panel output                                                 |
| :ki               | ->  | :i         |                       |                  | outputxyz text panel input                                                  |

* \*: Relay Direction, the direction in which data flows between global and local networks
* \*2: empty row indicates a memory chip, if RD is present in the correctly oriented memory relay or otherwise in a normal socket

### Ship coordinate space definition

* X+ forward
* Y+ right
* Z+ down

### Grid display layers

* 0: Background
* 1: Text
* 2: Cursor
* 3: Warnings

### feature interest

* full compass display: 3
* stats on grid display: 4
* small text panel: 3
* progress bar compass: 1
* autoalign: 4
* autopilot: 2
* fallback if ISAN receiver fails (out of ISAN range): 1
