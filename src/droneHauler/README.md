# Overview
* [Download Yolol scripts](https://gitlab.com/Firestar99/yolol/-/jobs/artifacts/master/browse/build/yolol/droneHauler/?job=build)

## Workflows
Decide the general drone behaviour and is set in `:w`

| Workflows | Description   |
| --------- | ------------- |
| 0         | idle          |
| 1         | grab asteroid |
| 2         | follow        |

## Functions
Reusable functions used multiple times in different workflows with differnent parameters. 
Functions are Void therefore not having return values, however they may of course change global variables as they please.
The current function is set in `:s`, as for state. Parameters are set with `:a` and `:b`.
A function assigning `:d=functionId` shows it is finished. The caller has to loop until `:d==functionId` to wait for the function execution to be completed. 
Note: Functions need to wait at least 2 Ticks after setting `:d=functionId` before they can start to wait for `:s==functionId` to run again because otherwise the controller may occasionally set the next function id to :s not fast  enough.
Some functions may continously run in the background after signaling execution completion continously causing desired side effects.
These function can't share a YOLOL chip with other functions as another function may be executed immediately. Therefore they have to be placed in seperate chips.

| id  | Description            | Parameters                        | b                                                  | Description                                                              | aftereffects |
| --- | ---------------------- | --------------------------------- | -------------------------------------------------- | ------------------------------------------------------------------------ | ------------ |
| 0   | wait                   | -                                 | -                                                  | -                                                                        | no           |
| 1   | aim                    | a=turnspeed for orientation       | boolean: ensure Rangefinder hit target             | -                                                                        | no           |
| 2   | fly to destination     | a=desired range                   | boolean: 1->use Rangefinder,0->use LaserDesignator | -                                                                        | no           |
| 3   | grab                   | None                              | -                                                  | -                                                                        | no           |
| 4   | release                | None                              | -                                                  | -                                                                        | no           |
| 5   | Aim AND fly            | a=turnspeed for orientation       | number: desired range to target                    | -                                                                        | no           |
| 6   | align to delivery line | -                                 | -                                                  | -                                                                        | no           |
| 7   | target Selector        | Identifier: key + designator name | Identifier: key + designator name                  | assign and wait for new designator. Toggles between two targets if b!="" | yes          |


## Constant Variables
Constants are numbers and many of them won't be changed by internal workflows. 
However some are not meant to be changed as this would otherwise break the program as shown in the table below. 
All constants are located in memory chips. 
They are mostly ranges, and ship speed values.

| Constants | Description                                                                                                                                                                    | Default | Changeable |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------- | ---------- |
| :ar       | desired asteroid approach range, provided to function 2 in parameter :a on asteroid approach                                                                                   | 5       | yes        |
| :rr       | desired reset range, provided to function 2 in parameter :a on reset to distance from asteroid after release                                                                   | 15      | yes        |
| :sr       | desired ship approach range, provided to function 2 in parameter :a on ship approach with an an asteroid to stay at a resonable distance until allowed to deliver the asteroid | 30      | yes        |
| :dr       | desired asteroid deliver range, provided to function 2 in parameter :a on delivery point approach with asteroid                                                                | 10      | yes        |
| :fr       | desired follow range, provided to function 2 in parameter :a when following the mothership                                                                                     | 30      | yes        |
| :tsu      | turn speed for Orientation for not carrying asteroid (unloaded)                                                                                                                | 5       | yes        |
| :tsa      | turn speed for Orientation for carrying asteroid                                                                                                                               | 10      | yes        |
| :tso      | turn speed for Orientation for overweight                                                                                                                                      | 50      | yes        |
| :do       | range finder to laser designator range allowed offset                                                                                                                          | 5       | yes        |

## Identifier Variables

Identifiers are Strings and all of them are constants. All dynamic numbers are located in memory chips. They are mostly Identifiers or part of identifiers and are assigned to devices during workflows.

| Name | Description                                                                                           |
| ---- | ----------------------------------------------------------------------------------------------------- |
| :ad  | asteroid designator name                                                                              |
| :dd  | delivery point designator name                                                                        |
| :cd  | center designator name                                                                                |
| :rd  | remote designator name                                                                                |
| :key | designator prefix and ground designator name. By default same for destination designator and receiver |

## Dynamic Number Variables

Dynamic numbers are numbers wich get changed workflow progression. All dynamic numbers are located in memory chips.
dynamic numbers represent internal states and are not meant to be changed by users manually. 
Therefore, you should only provide the initial values from the table below.
However if remote contact to the drone is interrupted, manually changing state `:s`, finishing the current function with `:d` or entire workflow `:w` and forcing a reset by setting the `:c` flag might be neccessary.

| Name | Description                | Initial |
| ---- | -------------------------- | ------- |
| :d   | current done state         | 0       |
| :s   | state                      | 0       |
| :w   | current workflow           | 0       |
| :dl  | lateral distance for lPID  | 0       |
| :dv  | vertical distance for vPID | 0       |
| :pd  | Pid distance               | 0       |

## Internal Flag Variables

Flags have only two states. All flags are located in memory chips.
These flags represent internal states and are not meant to be changed by users manually. 
Therefore, you should only provide the initial values from the table below.

| Flags | Description                                                       | Off State | On State | Initial |
| ----- | ----------------------------------------------------------------- | --------- | -------- | ------- |
| :mp   | activate mPID                                                     | 0         | 1        | 0       |
| :xp   | activate xPID                                                     | 0         | 1        | 0       |
| :yp   | activate yPID                                                     | 0         | 1        | 0       |
| :lp   | activate lPID                                                     | 0         | 1        | 0       |
| :vp   | activate vPID                                                     | 0         | 1        | 0       |
| :mf   | mPid done                                                         | 0         | 1        | 0       |
| :xf   | xPid done                                                         | 0         | 1        | 0       |
| :yf   | yPid done                                                         | 0         | 1        | 0       |
| :lf   | lPid done                                                         | 0         | 1        | 0       |
| :vf   | vPid done                                                         | 0         | 1        | 0       |
| :da   | delivery allowed                                                  | 0         | 1        | 0       |
| :c    | cancels current workflow 0:continue, -99: jump to start and reset | 0         | -99      | 0       |
| :p    | pause and remain in current function/workflow                     | 0         | 1        | 0       |
| :md   | use rangefinder for mPid, use laser designator otherwise          | 0         | 1        | 0       |
| :cb   | cargo beams active                                                | 0         | 1        | 0       |

## Device Variables

Global variables set inside devices. Initial values should remain default.

| Name | Device                  | Field               | Description                                                                    |
| ---- | ----------------------- | ------------------- | ------------------------------------------------------------------------------ |
| :rf  | range finder            | RangeFinderDistance | range finder current distance                                                  |
| :dn  | destination torpedo     | Identifier          | designator name                                                                |
| :dx  | destination torpedo     | OffsetX             | Pitch to target                                                                |
| :dy  | destination torpedo     | OffsetY             | Yaw to target                                                                  |
| :dr  | destination torpedo     | MissileDist         | distance to target                                                             |
| :ms  | Flight Control Unit     | FcuForward          | move speed                                                                     |
| :bs  | Flight Control Unit     | FcuBackward         | backwards speed                                                                |
| :xs  | Flight Control Unit     | FcuRotationalPitch  | Y axis speed                                                                   |
| :ys  | Flight Control Unit     | FcuRotationalYaw    | X axis speed                                                                   |
| :vs  | Flight Control Unit     | FcuUpDown           | vertical strafe speed                                                          |
| :ls  | Flight Control Unit     | FcuRightLeft        | lateral strafe speed                                                           |
| :o   | Ship Diagnostic Scanner | DurabilityErrors    | number of durability errors set by diagnostic scanner aka. the overweight flag |

## State Parameters

State parameters are shared among functions. A value must be assigned to parameters on function call. There are no default values and parameters will no be reseted after function end so they have to be overwritten.

| State Parameters |
| ---------------- |
| :a               |
| :b               |

## Signals

Signals from the Reicever result in Value specific Actions changing workflows or pausing them. 

| Value | Code | Action                                                  |
| ----- | ---- | ------------------------------------------------------- |
| 0     | 100  | abort current workflow and start grab asteroid workflow |
| 1     | 101  | abort current workflow and start follow workflow        |
| 2     | 110  | allow asteroid delivery(set :aw=1)                      |
| 3     | 111  | forbid asteroid delivery(set :aw=0)                     |


## Receiver
| Name   | Device               | Field        | Description                                           |
| ------ | -------------------- | ------------ | ----------------------------------------------------- |
| :comrm | Memory chip          |              | message received, overridden when new message arrives |
| :comrl | Torpedo Launcher End | LauncherLock | preventing torpedo from firing, just in case          |
| :comri | Torpedo Laser Sensor | Identifier   | setting identifier                                    |
| :comrx | Torpedo Laser Sensor | OffsetX      | incoming bit, backup mode for deadzone                |
| :comry | Torpedo Laser Sensor | OffsetY      | incoming bit, backup mode for deadzone                |
| :comrd | Torpedo Laser Sensor | MissileDist  | incoming bit, main mode of transfer                   |