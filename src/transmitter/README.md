# Transmitter

* [Blueprint download](https://gitlab.com/Firestar99/yolol/-/blob/master/src/transmitter/transmitter.fbe)
* [Video showcase](https://youtu.be/9CQO0LWadUk) (of an older version)

## Introduction

A **network** is a collection of ships, derelicts, objects which want to communicate with each other. The maximum range of communication between is 1000m limited by the
maximum range laser designators work in the game. This range is measured from the transmitter to the receiver as well as from the ground point to the receiver, if any of
these distances are larger than 1000m communication will fail.

A network has one single unique **key** of type string which acts as its password. When deploying a network in the universe one should change the key of all participants
to a private key, a randomly generated sequence of at least 16 characters is recommended. This prevents two identical systems from interfering with each other as well as
prevent malicious actors from activities such as listening to sent messages, sending their own to disrupt the system or locating any senders (up to a range of 1000m). By
default, the key is stored in a memory chip on a ship's network with the name `:comk`. A key is always required, an empty key will not function due to Laser Designator
limitations.

A **channel** is an identifier which needs to be equal on a transmitter and receiver so that they can talk to each other. One network can have multiple channels of
communication on which messages can be sent independently. The identifier of a channel is a string, examples being "0", "mytest" or "universe42". The message sent across
a channel has to be a positive number (including 0) and has a maximum possible number is computed using `2^bitwidth`. The bitwidth is a per channel constant of which
every transmitter and receiver on said channel has to know in advance. A higher bitwidth allows for larger numbers to be messaged but also increases the time it takes per
message, which can be calculated by `0.6s + bitwidth * 0.4s`. The default bitwidth of 4 allows for numbers between 0 and 15 to be sent and it takes 2.2s for the message
to arrive. If one sents a number larger than allowed it will be `% (2^bitwidth)` and if one sends a number below 0 it will not be sent. For correct operation a channel
can only have 1 transmitter sending to it, but multiple receivers getting the message, if multiple transmitters would send on a single channel they could send a message
at the same or similar time disrupting each other's message.

Depending on ground and transmitter position a deadzone may be created. Geometrically this is a plane right inbetween the ground and transmitter with even a certain width
to it which increases with distance to transmitter/ground and decreases with distance between transmitter and ground. Actual measurements of the deadzone have not yet
been performed.

## Ground

The common ground point is a physical device which defines the 0 point of a network. At least one ground point must be present in a network to be able to function. All
channels will share the one ground point for communication. Multiple ground points may be required if a network operates in a larger range (1000m) than a single laser
designator can cover, however such a system has not yet been tested.

Prefix `:comg` = `:com` (communication) + `g` (ground)

| Name   | Device           | Field        | Description            |
|--------|------------------|--------------|------------------------|
| :comk  | Memory chip      |              | the key of the network |
| :comga | Laser designator | Active       | turning on             |
| :comgi | Laser designator | IdentifierId | setting identifier     |

## Transmitter

A device which **sends** a message. It consists out of a Laser Designator and a basic Yolol chip containing an optimized version of `transmitter.yolol`, with any empty
lines preserved as is. It is cheap to build at about 0.2 Agesium, 0.3 Charodium, 0.1 Surtrite Crystal and 2000$ Manufacturing and Assembly cost. It also requires a free
slot in any memory chip with a variable name of `:comtm`.

If one wants to send a message, one should write it to `:comtm` if and only if `:comtm == -1`. With everything idle the transmitter yolol chip should pick up the message
within 0.2s, reset `:comtm` to `-1` again and start sending out the message. If however messages come in faster than they can be sent out, `:comtm` will remain at the
to-be sent message until the transmitter yolol chip is ready to pick it up. In this case any external chip wanting to send something should loop and wait
until `:comtm == -1` and only then write its message to it. If it is known that such a backlog cannot happen or one can accept the dropping of messages, one may ignore
this recommendation.

Prefix `:comt` = `:com` + `t` (transmitter)

| Name   | Device           | Field        | Description                 |
|--------|------------------|--------------|-----------------------------|
| :comk  | Memory chip      |              | the key of the network      |
| :comtm | Memory chip      |              | message to send, -1 if idle |
| :comta | Laser designator | Active       | turning on                  |
| :comtl | Laser designator | LaserLength  | sending bits [0, 1]         |
| :comti | Laser designator | IdentifierId | setting identifier          |

## Receiver

A device which **receives** a message. It is build from a Torpedo setup with a Torpedo Launcher End, Torpedo Launcher Front, Torpedo Main Thruster, Torpedo Laser Sensor
and a basic Yolol chip containing an optimized version of `transmitter.yolol`. It is the most expensive component and requires about 0.2 Ymrium, 0.1 Kutonium and a
Manufacturing cost of about 28.000$ each, but also far down the research tree. It also requires a free slot in any memory chip with a variable name of `:comrm`.

When a message is received it is accumulated and once the entire message was fully received it is written to `:comrm`, overriding any previous value. It is assumed that
either `:comrm` is used as a status variable so the new state can just override the old one or any chip accepting and processing the incoming message is faster than the
receiver.

Prefix `:comr` = `:com` + `r` (receiver)

| Name   | Device               | Field        | Description                                           |
|--------|----------------------|--------------|-------------------------------------------------------|
| :comk  | Memory chip          |              | the key of the network                                |
| :comrm | Memory chip          |              | message received, overridden when new message arrives |
| :comrl | Torpedo Launcher End | LauncherLock | preventing torpedo from firing, just in case          |
| :comri | Torpedo Laser Sensor | Identifier   | setting identifier                                    |
| :comrx | Torpedo Laser Sensor | OffsetX      | incoming bit, backup mode for deadzone                |
| :comry | Torpedo Laser Sensor | OffsetY      | incoming bit, backup mode for deadzone                |
| :comrd | Torpedo Laser Sensor | MissileDist  | incoming bit, main mode of transfer                   |
